<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Devices extends MY_Controller {

	public $user_id;
	public $account_id;

	public function __construct() {
		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('device_imei', 'device_mobile', 'assigned_users', 'last_sync', 'status', );
		parent::__construct();
		$this -> fields['ft_data'] = array('plugins/dataTables/jquery.datatables.min.js', 'custom/custom-datatable.js');

		if (!$this -> session -> userdata('is_login') == TRUE) {
			$this -> session -> set_userdata('login_error', 'Please, Loggin first');
			redirect('login');
		}

		$this -> load -> model('devices_model');
		$this -> load -> model('employees_model');

		$this -> devices_model -> form_validation();
		$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');
		$this -> fields['view'] = 'devices/view/';
		$this -> fields['edit'] = 'devices/index/update/';
		$this -> fields['add_new'] = 'devices/new_device/';
		$this -> fields['delete'] = 'devices/delete/';

		$this -> fields['table_id'] = $this -> devices_model -> table_id;

		$this -> fields['tb_name'] = 'all_devices';
		$this -> fields['controller'] = 'devices';
		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');

		//filtering the property items according to companyies

		/******************** check user permissions ***************************/
		if (!in_array(4, $this -> user_permissions)) {

			redirect('login/logout');
		}

		/******************** check user permissions ***************************/
	}

	public function index() {

		$this -> fields['controller'] = 'devices';

		$where = array('devices.account_no' => $this -> account_id);
		$this -> fields['tb_data'] = $this -> devices_model -> read_devices($where);

		if ($this -> fields == FALSE) {
			$this -> data['tb_data'] = array();
		} else { $this -> data['tb_data'] = $this -> fields;
		}

		$where_emp = array('account_no' => $this -> account_id);
		$employees = $this -> employees_model -> read($where_emp);
		//echo '<pre>';print_r($employees);

		$employee = array('' => 'Assign Device');
		if ($employees) {
			foreach ($employees as $emp) {
				$employee[$emp['employee_id']] = $emp['firstname'] . ' ' . $emp['lastname'];
			}

		}
		$this -> fields['employees'] = $employee;

		//updating device information

		if ($this -> uri -> segment(3) == 'update') {

			$where = array('device_id' => $this -> uri -> segment(4));
			$info = $this -> devices_model -> read($where);
			if ($info) {
				foreach ($info as $info) {
				}
				$this -> fields['info'] = $info;
			}

		} elseif ($this -> uri -> segment(3) == 'delete') {
			$this -> fields['info'] = FALSE;
			$id = $this -> uri -> segment(4);
			$data = array('device_status' => 0);
			$this -> devices_model -> update($id, $data);
		} else {

			$this -> fields['info'] = FALSE;
		}

		//checking users permissions
		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/device_table');
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');

	}

	//viewing
	public function view() {

		$this -> fields['row_fields'] = $this -> fields['tb_headers'] = array('device_id', 'device_imei', 'device_descr', 'assigned_to', 'created_on', 'created_by', 'last_update', 'account_no', 'device_status', 'device_mobile');

		$id = $this -> uri -> segment(4);

		$where = array('devices.id' => $id);
		//geting the data
		$emp = $this -> devices_model -> read_device($where);
		if ($emp) {
			foreach ($emp as $emp) {
				$this -> fields['tb_data'] = $emp;
			}
		} else {
			$this -> fields['tb_data'] = FALSE;
		}

		$this -> load -> view('template/header', $this -> fields);
		$this -> load -> view('template/content/device_view', $this -> fields);
		$this -> load -> view('template/footer');

	}

	public function create_device() {

		if ($this -> form_validation -> run() != FALSE) {
			extract($_POST);

			$data = array('device_imei' => $device_imei, 'device_descr' => $device_descr, 'assigned_to' => $assigned_to, 'created_on' => date('Y-m-d h:i:s'), 'created_by' => $this -> user_id, 'assigned_on' => date('Y-m-d h:i:s'), 'account_no' => $this -> account_id, 'device_mobile' => $device_mobile);

			$this -> devices_model -> save($data);

			redirect('devices');
		} else {
			$this -> session -> set_userdata('error', 'Error! Imei arleady used');
			redirect('devices');
		}
	}

	public function update_device() {

		if ($this -> form_validation -> run() != FALSE) {
			extract($_POST);

			$data = array('device_imei' => $device_imei, 'device_descr' => $device_descr, 'assigned_to' => $assigned_to, 'created_on' => date('Y-m-d h:i:s'), 'created_by' => $this -> user_id, 'assigned_on' => date('Y-m-d h:i:s'), 'account_no' => $this -> account_id, 'device_mobile' => $device_mobile);
			print_r($data);
			if ($this -> input -> post('device_id') == FALSE) {
				$this -> devices_model -> save($data);
			} else {
				$this -> devices_model -> update($device_id, $data);
			}
			redirect('devices');
		}
	}

	public function checkmail($email, $customer_id) {
		if ($customer_id) {
		} else {$customer_id = 'no';
		}
		$where = array('devices.id' => $customer_id, 'email' => $email, 'created_by' => $this -> session -> userdata('account'));

		$customer = $this -> devices_model -> read($where);

		if ($customer) {
			return TRUE;
		} else {
			$where = array('devices.email' => $email);
			$customer = $this -> devices_model -> read($where);
			if ($customer) {
				$this -> form_validation -> set_message('checkmail', 'Sorry! the email is already used by someone else');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function checkmobile($mobile, $customer_id) {
		if ($customer_id) {
		} else {$customer_id = 'no';
		}
		$where = array('mobile' => $mobile, 'devices.id' => $customer_id, 'created_by' => $this -> session -> userdata('account'));

		$customer = $this -> devices_model -> read($where);

		if ($customer) {
			return TRUE;
		} else {

			$where = array('devices.mobile' => $mobile);
			$customer = $this -> devices_model -> read($where);
			if ($customer) {
				$this -> form_validation -> set_message('checkmobile', 'Sorry! the mobile number is already used by someone else');
				return FALSE;
			} else {
				return TRUE;
			}
		}

	}

	//check location code
	public function checkimei($imei, $id) {
		echo $id;
		$device = $this -> devices_model -> read(array('device_imei' => $imei));
		if ($device) {
			if ($device[0]['device_id'] == $id) {

				return TRUE;
			} else {
				$this -> form_validation -> set_message('checkimei', 'Sorry! the Device Imei is already exist');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}

	public function delete() {
		$id = $this -> uri -> segment(4);
		$act = $this -> uri -> segment(3);
		if ($act == 'add') {
			$data = array('device_status' => 1);
		} elseif ($act == 'remove') {
			$data = array('device_status' => 0);
		}
		if ($this -> devices_model -> update($id, $data)) {
			redirect('devices');
		}
	}

	public function reset($id) {
		//read device
		$device = $this -> devices_model -> read(array('device_id' => $id));
		if ($device) {

			$data = array('last_sync' => '0000-00-00 00:00:00');
			$this -> devices_model -> update($id, $data);

			//reset the device sync status
			$this -> devices_model -> upd_dev_status($device[0]['device_imei']);

		}
		redirect('devices');
	}

}
