<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Reports extends My_Controller {
	public $user_id;
	public $account_id;

	public function __construct() {
		parent::__construct();

		if ($this -> session -> userdata('is_login') == FALSE) {
			redirect('login');
		}

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('survey_id', 'survey_title', 'key_word', 'descriptions', 'no_questions', 'cap_size', 'start_date', 'end_date', 'status');
		//$this->data['row_fields'] = $this->data['tb_headers'] = array('responder_id', 'responder_mobile', 'survey_title', 'location_name', 'responce_date', 'responce_status', );

		$this -> load -> model('reports_model');
		$this -> load -> model('questionaires_model');
		$this -> load -> model('surveys_model');
		$this -> load -> model('locations_model');
		$this -> load -> model('devices_model');

		$this -> data['table_id'] = $this -> reports_model -> table_id;

		//$this -> reports_model -> form_validation();
		//$this -> form_validation -> set_error_delimiters('<div class="alert alert-block span4">', '</div>');

		//print_r($this->session->all_userdata());
		$this -> user_id = $this -> session -> userdata('user_id');
		$this -> account_id = $this -> session -> userdata('account_id');

		$this -> data['controller'] = 'reports';
		$this -> data['edit'] = 'reports/update_location';
		$this -> data['view'] = 'reports/view';
		$this -> data['delete'] = 'reports/delete';

		/*
		 * load location on default
		 */
		$locations = $this -> locations_model -> read(array('account_no' => $this -> account_id));
		$locate = array('' => 'Select a Location');
		if ($locations) {
			foreach ($locations as $key => $value) {
				$locate[$value['location_code']] = $value['location_name'];
			}
		} else { $locate = array('' => 'No  Locations');
		}
		$this -> data['locations'] = $locate;

		//interviewe data

		$interviewer = $this -> devices_model -> read(array('account_no' => $this -> account_id));

		$interv = array('' => 'Select Interviewer');
		if ($interviewer) {

			foreach ($interviewer as $interviewer) {
				$interv[$interviewer['device_imei']] = $interviewer['device_imei'];
			}
		}
		$this -> data['interviewer'] = $interv;

	}

	public function printX() {

		//surveys
		$data = $this -> reports_model -> get_participants(array('participants.survey_no' => 1), 100, 0);
		$this -> exportingToExcel($data);

	}

	public function index() {
		$this -> session -> unset_userdata('id');
		//surveys
		$surveys = $this -> surveys_model -> read(array('account_no' => $this -> account_id));
		if ($surveys) {
			$surv = array('' => 'Select Survey');
			foreach ($surveys as $surveys) {
				$surv[$surveys['survey_id']] = $surveys['survey_title'];
			}
		} else {
			$surv[''] = 'No Survyes Avalable';
		}

		$location = $this -> locations_model -> read(array('account_no' => $this -> account_id));
		if ($location) {
			$locations = array('' => 'Select Facility');
			foreach ($location as $location) {
				$locations[$location['location_id']] = $location['location_name'];
			}
		} else {
			$locations[''] = 'No Facilities Avalable';
		}

		$this -> data['surveys'] = $surv;
		$this -> data['locations'] = $locations;

		$where = array('surveys.account_no' => 1, 'surveys.status' => 1);
		//filtering data

		if ($this -> input -> post('filter')) {
			extract($_POST);

			if ($survey_title) {
				$where['survey_id'] = $survey_title;
			}
			if ($survey_title == FALSE) {
				$where = FALSE;
			}
		}
		$surveys = $this -> surveys_model -> read($where);

		if ($surveys) {
			$this -> data['tb_data'] = $surveys;
		} else {
			$this -> data['tb_data'] = FALSE;
		}

		$this -> data['tb_name'] = 'surveys_report';

		$this -> data['stc_active'] = 'class="active"';
		$this -> data['add_btn'] = 'add_new_stock';

		$this -> data['pagenate'] = FALSE;

		$this -> data['pagetitle'] = 'Surveys List';
		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/reports_table');
		$this -> load -> view('template/footer');

	}

	/*
	 * reports by survey
	 */
	public function survey() {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('survey_title', 'start_date', 'end_date', 'total_response', 'status');

		if ($this -> input -> post('filter')) {

			extract($_POST);

			if ($location_name) {
				$where['location_name'] = $location_name;
			}

		}

		$surveys = $this -> surveys_model -> read(array('account_no' => $this -> account_id));

		if ($surveys) {
			$surv = array('' => 'Select Survey');

			$this -> data['tb_data'] = $surveys;

		} else {
			$this -> data['tb_data'] = 'No Survyes Avalable';
		}

		$summary = $this -> reports_model -> read_surveys_summary();
		if ($summary) {
			$this -> data['summary'] = $summary;
		} else {
			$this -> data['summary'] = FALSE;
		}

		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/reports_survey_table', $this -> data);
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');

	}

	public function survey_report($survey) {

		if ($this -> input -> post() || $survey) {

			extract($_POST);
			$where = array('participants.survey_no' => $survey);
			if ($this -> input -> post() != FALSE) {
				extract($_POST);
				if ($location) {
					$where['participants.location_code'] = $location;
				}
				if ($pmtcp_status) {
					//	$where['participants.pmtcp_status'] = $pmtcp_status;
				}
				if ($daterange) {
					$where['participants.interview_date'] = $daterange;
				}
				if ($interviewer) {
					$where['participants.device_imei'] = $interviewer;
				}
			}
			//get su
			$this -> data['row_fields'] = $this -> data['tb_headers'] = array('survey_no', 'interview_date', 'location_code', 'participant_no', 'interviewer_no', 'start_time', 'end_time');

			//get location details
			$survey = $this -> surveys_model -> read(array('survey_id' => $survey));

			if ($survey) {
				//print_r($survey);
				$this -> data['survey'] = $survey[0];

				$survey_responces = $this -> reports_model -> get_participants($where);
				//print_r($survey_responces);
				if ($survey_responces) {

					$this -> data['tb_data'] = $survey_responces;
				} else {
					$this -> data['tb_data'] = FALSE;
				}

				$this -> data['totals'] = $this -> read_summary($where);
				$this -> data['controller'] = 'reports/survey_report';
				$this -> load -> view('template/header', $this -> data);
				$this -> load -> view('template/content/survey_report_view');
				$this -> load -> view('template/table_helper');
				$this -> load -> view('template/footer');
			} else {
				redirect('reports/survey');
			}
		} else {
			redirect('reports/survey');
		}
	}

	/*
	 * surveys reports ends
	 *
	 */

	public function view_responce() {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('responder_mobile', 'survey_title', 'responce_date', 'location_name', 'responce_status');
		$where = array('responder_id' => $this -> uri -> segment(4));

		$data = $this -> surveys_model -> read_responces($where);
		if ($data) {
			foreach ($data as $data) {
				$this -> data['info'] = $data;
			}

			$where_s = array('survey_no' => $data['survey_no']);
			$this -> data['questions'] = $this -> questionaires_model -> read($where_s);
			$where_me = array('survey_no' => $data['survey_no'], 'responder' => $data['responder_mobile']);
			$replies = $this -> surveys_model -> read_my_responces($where_me);
			if ($replies) {
				$this -> data['replies'] = $replies;
			} else {
				$this -> data['replies'] = FALSE;
			}
			$this -> data['stc_active'] = 'class="active"';
			$this -> data['add_btn'] = 'add_new_stock';

			$this -> load -> view('template/header', $this -> data);
			$this -> load -> view('template/content/view_responce');
			$this -> load -> view('template/table_helper');
			$this -> load -> view('template/footer');
		} else {
			redirect('reports');
		}

	}

	/*
	 * printing to excel starts
	 */

	public function get_data_to_export() {
		$where = array('survey_no' => $this -> uri -> segment(4));
		if ($this -> session -> userdata('location') != 'all') {
			$where['location_code'] = $this -> session -> userdata('location');
		}
		$data = $this -> reports_model -> get_data_to_export($where);

		$head = array('responder', 'survey_no', 'location_code', 'responce_date', 'my_answers');

		$responces = array();
		if ($data) {
			foreach ($data as $respo) {

				$dd = array();
				foreach ($head as $title) {
					/*if ($title == 'my_questions') {
					 } else */

					if ($title == 'my_answers') {

						$ans = explode('|', $respo[$title]);

						for ($q = 1; $q <= $respo['no_questions']; $q++) {

							//compare ansers to questions
							$qn = $q - 1;
							//question number

							if (key_exists($qn, $ans)) {
								//check was selected else 99=null
								if ($ans[$qn]) {
									$ansz = explode(',', $ans[$qn]);

									$a = 'A';
									foreach ($ansz as $key => $value) {
										$dd['Q' . $q . $a] = trim($value);
										$a++;
									}
								} else {
									$dd['Q' . $q] = 99;
								}
							} else {
								$dd['Q' . $q] = 99;
							}
						}

					} else {
						$dd[$title] = $respo[$title];
					}
				}

				$responces[] = $dd;

			}
			echo "<pre>";
			print_r($responces);

			print_r($data);
			//	$this -> exporting($responces);
		} else {

			//redirect('reports');
		}
	}

	public function get_open_responces($location) {
		$where = array('survey_no' => $this -> settings['active_survey'], 'qn_type' => 'open_ended');

		//get all open ended questios
		$questions = $this -> questionaires_model -> read_open($where);
		$where_in = array();
		if ($questions) {
			foreach ($questions as $key => $value) {
				$where_in[] = $value['rank'];
			}
		}

		$where = array('survey_no' => $this -> settings['active_survey'], 'location_code' => $location);
		$data = $this -> reports_model -> get_open_responces($where, $where_in);

		$head = array('responder', 'survey_no', 'location_code', 'responce_date', 'responce');

		$responces = array();
		if ($data) {
			foreach ($data as $respo) {

				$dd = array();
				foreach ($head as $title) {
					$dd[$title] = $respo[$title];
				}
				$responces[] = $dd;
			}
			$this -> exportingToExcel($responces);
		}

	}

	/*
	 * location report start
	 */

	public function locations() {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('location_code', 'location_name', 'total_response');

		$locations = $this -> locations_model -> read(array('account_no' => $this -> account_id));
		if ($locations) {
			$this -> data['tb_data'] = $locations;
		} else {
			$this -> data['tb_data'] = FALSE;
		}
		$summary = $this -> reports_model -> read_locations_summary();
		if ($summary) {
			$this -> data['summary'] = $summary;
		} else {
			$this -> data['summary'] = FALSE;
		}
		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/location_report');
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');
	}

	public function location_report($location) {

		//the surveyor id

		if ($this -> input -> post() || $location) {
			$where = array('participants.location_code' => $location);
			if ($this -> input -> post() != FALSE) {
				extract($_POST);
				if ($location) {
					$where['participants.location_code'] = $location;
				}
				if ($pmtcp_status) {
					//$where['participants.pmtcp_status'] = $pmtcp_status;
				}
				if ($daterange) {
					$where['participants.interview_date'] = $daterange;
				}
				if ($interviewer) {
					$where['participants.device_imei'] = $interviewer;
				}
			}

			$this -> data['row_fields'] = $this -> data['tb_headers'] = array('survey_no', 'interview_date', 'location_code', 'participant_no', 'interviewer_no', 'start_time', 'end_time');

			//get location details
			$location = $this -> locations_model -> read(array('account_no' => $this -> account_id));
			if ($location) {
				$this -> data['location'] = $location[0];

				$location_responces = $this -> reports_model -> get_participants($where);
				if ($location_responces) {

					$this -> data['tb_data'] = $location_responces;
				} else {
					$this -> data['tb_data'] = FALSE;
				}
				//get summary
				$this -> data['totals'] = $this -> read_summary($where);

				$this -> data['controller'] = 'reports/location_report';
				$this -> load -> view('template/header', $this -> data);
				$this -> load -> view('template/content/location_report_view');
				$this -> load -> view('template/table_helper');
				$this -> load -> view('template/footer');
			} else {
				redirect('reports/locations');
			}
		} else {
			redirect('reports/locations');
		}
	}

	/*
	 * location report end
	 */

	public function surveyor() {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('device_imei', 'employee_name', 'total_response');

		$surveyors = $this -> devices_model -> read_devices(array('devices.account_no' => $this -> account_id));
		if ($surveyors) {

			$this -> data['tb_data'] = $surveyors;
		} else {
			$this -> data['tb_data'] = FALSE;
		}
		$summary = $this -> reports_model -> read_surveyor_summary();

		if ($summary) {
			$this -> data['summary'] = $summary;
		} else {
			$this -> data['summary'] = FALSE;
		}
		$this -> load -> view('template/header', $this -> data);
		$this -> load -> view('template/content/surveyor_report_table');
		$this -> load -> view('template/table_helper');
		$this -> load -> view('template/footer');
	}

	public function surveyor_report($surveyor) {

		//the surveyor id
		if ($this -> input -> post() || $surveyor) {
			$where = array('device_imei' => $surveyor);
			if ($this -> input -> post() != FALSE) {
				extract($_POST);
				if ($location) {
					$where['participants.location_code'] = $location;
				}
				if ($pmtcp_status) {
					$where['participants.pmtcp_status'] = $pmtcp_status;
				}
				if ($daterange) {
					$where['participants.interview_date'] = $daterange;
				}
			}
			//get surveyor details

			$surveyor = $this -> devices_model -> read_devices(array('device_imei' => $surveyor));
			if ($surveyor) {
				$this -> data['surveyor'] = $surveyor[0];
				$this -> data['row_fields'] = $this -> data['tb_headers'] = array('survey_no', 'interview_date', 'location_code', 'participant_no', 'interviewer_no', 'device_imei', 'start_time', 'end_time');

				$surveyor_reponses = $this -> reports_model -> get_participants($where);
				if ($surveyor_reponses) {

					$this -> data['tb_data'] = $surveyor_reponses;
				} else {
					$this -> data['tb_data'] = FALSE;
				}

				$this -> data['totals'] = $this -> read_summary($where);
				$this -> data['controller'] = 'reports/surveyor_report';
				$this -> load -> view('template/header', $this -> data);
				$this -> load -> view('template/content/surveyor_report_view');
				$this -> load -> view('template/table_helper');
				$this -> load -> view('template/footer');
			} else {
				redirect('reports/surveyor');
			}
		} else {
			redirect('reports/surveyor');

		}
	}

	/*
	 * view participants data
	 */

	public function participant($survey, $participant) {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('question_rank', 'question_text', 'answer_choices');

		//get participant details

		$where = array('participant_id' => $participant);
		$participant = $this -> reports_model -> get_participants($where);

		if ($participant) {

			//participant data
			foreach ($participant as $participant) {

			}
			$this -> data['participant'] = $participant;

			//survey details
			$where = array('survey_id' => $survey);

			$surveys = $this -> surveys_model -> read($where);

			if ($surveys) {

				foreach ($surveys as $surveys) {

				}

				$this -> data['survey'] = $surveys;
				$where = array('survey_no' => $surveys['survey_id']);

				//read survey sections
				$this -> load -> model('survey_sections_model');
				$sections = $this -> survey_sections_model -> read($where);
				if ($sections) {

					$this -> data['sections'] = $sections;

				} else {
					$this -> data['sections'] = FALSE;
				}

				//get all questions

				$questions = $this -> questionaires_model -> read_questions(array('questionaires.survey_no' => $survey, 'language' => $this -> settings['default_language']));
				if ($questions) {

					//get answer choices

					$choices = $this -> questionaires_model -> read_answer_options(array('survey_no' => $survey));
					if ($choices) {
						$this -> data['choices'] = $choices;
					} else {
						$this -> data['choices'] = FALSE;
					}

					$this -> data['questions'] = $questions;
				} else {
					$this -> data['questions'] = FALSE;
				}
				//participant responses
				$where = array('participant_no' => $participant['participant_no'], 'device_imei' => $participant['device_imei']);

				$responses = $this -> reports_model -> read_responses($where);

				if ($responses) {
					$this -> data['responses'] = $responses;
				} else {
					$this -> data['responses'] = FALSE;
				}
				$this -> load -> view('template/header', $this -> data);
				$this -> load -> view('template/content/participant_view', $this -> data);
				$this -> load -> view('template/table_helper');
				$this -> load -> view('template/footer');
			} else {
				redirect('reports/survey_report/' . $survey);
			}

		} else {
			redirect('surveys');

		}

	}

	public function print_participant($survey, $participant) {

		$this -> data['row_fields'] = $this -> data['tb_headers'] = array('question_rank', 'question_text', 'answer_choices');

		//get participant details

		$where = array('participant_id' => $participant);
		$participant = $this -> reports_model -> get_participants($where);

		if ($participant) {

			//participant data
			$this -> data['participant'] = $participant[0];

			//survey details
			$where = array('survey_id' => $survey);

			$surveys = $this -> surveys_model -> read($where);

			if ($surveys) {

				foreach ($surveys as $surveys) {

				}

				$this -> data['survey'] = $surveys;
				$where = array('survey_no' => $surveys['survey_id']);

				//read survey sections
				$this -> load -> model('survey_sections_model');
				$sections = $this -> survey_sections_model -> read($where);
				if ($sections) {

					$this -> data['sections'] = $sections;

				} else {
					$this -> data['sections'] = FALSE;
				}

				//get all questions

				$questions = $this -> questionaires_model -> read(array('survey_no' => $survey));
				if ($questions) {

					//get answer choices

					$choices = $this -> questionaires_model -> read_answer_options(array('survey_no' => $survey));
					if ($choices) {
						$this -> data['choices'] = $choices;
					} else {
						$this -> data['choices'] = FALSE;
					}
					$this -> exportingToExcel($questions);
					$this -> data['questions'] = $questions;
				} else {
					$this -> data['questions'] = FALSE;
				}

				redirect('reports/survey_report/' . $survey . '/' . $participant);
			} else {
				redirect('reports/survey_report/' . $survey . '/' . $participant);
			}

		} else {
			redirect('reports/survey_report/' . $survey . '/' . $participant);
		}

	}

	public function printing($survey) {
		//$survey = $this -> settings['active_survey'];
		//$this->uri->segment(3);

		$where = array('questionaires.survey_no' => $survey,  'language' => $this -> settings['default_language']);

		$questions = $this -> questionaires_model -> read_questions_with_choices($where);

		$flag = array('participant_no', 'interviewer_no', 'survey_no', 'location', 'inverview_date', 'pmtct_status', 'anc_number', 'start_time', 'end_time');

		if ($questions) {
 
 

			$q = 1;
			$typeA = array("Closed_ended", "Partial_open_ended");
			$typeB = array("Open_ended", "Partial_open_ended_mult", "Closed_ended_mult","Partial_open_ended");

			foreach ($questions as $key => $value) {
				$choices = explode(',', $value['data_type']);

				//check question types
				if (in_array($value['question_type'], $typeB, TRUE)) {
					//displaying multi options

					$i = 'a';

					for ($c = 0; $c < count($choices); $c++) {

						//check data types
						if ($choices[$c] == 3) {// 3 => date
							$flag[] = $value['qn_numbering'] . $q . $i . '-y';
							$flag[] = $value['qn_numbering'] . $q . $i . '-m';
							$flag[] = $value['qn_numbering'] . $q . $i . '-d';

						} else {

							$flag[] = $value['qn_numbering'] . $q . $i;
						}

						$i++;
					}

				} else {
					if ($choices[0] == 3) {// 3 => date
						$flag[] = $value['qn_numbering'] . $q . '-y';
						$flag[] = $value['qn_numbering'] . $q . '-m';
						$flag[] = $value['qn_numbering'] . $q . '-d';

					} else {
						$flag[] = $value['qn_numbering'] . $q;

					}

				}

				$q++;
			}

		}

		$flag = array_flip($flag);

		//get participant responses
		$where = array('responses.survey_no' => $survey);
		$responses = $this -> reports_model -> responses_data($where);

		$participants = $myqn = array();
		if ($responses) {

			foreach ($responses as $response) {

				$data = explode('~', $response['answers_no']);
				$myqns = explode('~', $response['questions']);

				$myqns = array_flip($myqns);

				$q = $qn = 1;

				foreach ($questions as $question) {

					if (array_key_exists($question['question_rank'], $myqns)) {
						//display the answered value
 
						$zeqn = $data[$myqns[$question['question_rank']]];
 
						// remove the last separator

						$answr = explode('|', $zeqn);

						if (count($answr) > 1) {

							//for multiple options

							$ch = 'A';
							foreach ($answr as $ans) {

								//check if is date

								$jb = explode('-', $ans);
								if (count($jb) == 3) {
									$myqn['Q' . $qn . $ch . 'y'] = $jb[0];
									$myqn['Q' . $qn . $ch . 'm'] = $jb[1];
									$myqn['Q' . $qn . $ch . 'd'] = $jb[2];

								} else {
									$myqn['Q' . $qn . $ch] = $ans;

								}

								$ch++;
							}
						} else {

							$myqn['Q' . $qn] = $zeqn;
						}

					} else {

						//check question type
						if ($question['question_type'] == 'Closed_ended_mult' || $question['question_type'] == 'Partial_open_ended_mult' || $question['question_type'] == 'Partial_open_ended' || $question['question_type'] == 'Open_ended') {
							$dt = explode(',', $question['data_type']);

							$d = 'A';
							foreach ($dt as $dd) {

								if ($dd == 3) {
									$myqn['Q' . $qn . $d . 'y'] = 0;
									$myqn['Q' . $qn . $d . 'm'] = 0;
									$myqn['Q' . $qn . $d . 'd'] = 0;

								} else {
									$myqn['Q' . $qn . $d] = 0;

								}

								$d++;
							}

						} else {
							$dt = explode(',', $question['data_type']);

							if ($dt[0] == 3) {
								$myqn['Q' . $qn . 'y'] = 0;
								$myqn['Q' . $qn . 'm'] = 0;
								$myqn['Q' . $qn . 'd'] = 0;

							} else {
								$myqn['Q' . $qn] = '0';

							}
 
						}

					}
					$qn++;

				}
				//echo "<pre>";
				unset($response['answers_no']);
				unset($response['questions']);
				$responder = array_merge($response, $myqn);
				$participants[] = $responder;

			}
		}
		 
		/* echo "<pre>";
		  print_r($participants[0]);
		  echo "<table border=2><tr><td>";
		 echo "<pre>";
		
		 echo  count($participants[0]);
		 echo "</td><td>";
		 echo "<pre>";print_r($flag);
		 echo count($flag);
		 echo "</td></tr></table>";   */
		  $this -> exportingToExcel($participants, $flag);
	}

	public function printing12($survey) {
		//$survey = $this -> settings['active_survey'];
		//$this->uri->segment(3);
		echo "<pre>";
		$where = array('questionaires.survey_no' => $survey, 'language' => $this -> settings['default_language']);

		$questions = $this -> questionaires_model -> read_questions_with_choices($where);

		$flag = array('participant_no', 'interviewer_no', 'survey_no', 'location', 'inverview_date', 'pmtct_status', 'anc_number', 'start_time', 'end_time');

		if ($questions) {

			$q = 1;
			$typeA = array("Open_ended", "Closed_ended", "Partial_open_ended");
			$typeB = array("Partial_open_ended_mult", "Closed_ended_mult");

			foreach ($questions as $key => $value) {
				$choices = explode(',', $value['data_type']);

				//check question types
				if (in_array($value['question_type'], $typeB, TRUE)) {

					//displaying multi options

					$i = 'a';

					for ($c = 0; $c < count($choices); $c++) {

						//check data types
						if ($choices[$c] == 3) {// 3 => date
							$flag[] = $value['qn_numbering'] . $q . $i . '-y';
							$flag[] = $value['qn_numbering'] . $q . $i . '-m';
							$flag[] = $value['qn_numbering'] . $q . $i . '-d';

						} else {

							$flag[] = $value['qn_numbering'] . $q . $i;
						}

						$i++;
					}

				} else {
					if ($choices[0] == 3) {// 3 => date
						$flag[] = $value['qn_numbering'] . $q . '-y';
						$flag[] = $value['qn_numbering'] . $q . '-m';
						$flag[] = $value['qn_numbering'] . $q . '-d';

					} else {
						$flag[] = $value['qn_numbering'] . $q;

					}

				}

				$q++;
			}

		}

		$flag = array_flip($flag);

		//get participant responses
		$where = array('responses.survey_no' => $survey);
		$responses = $this -> reports_model -> responses_data($where);

		//prepare responses data
		$participants = $myqn = array();
		if ($responses) {
			foreach ($responses as $key => $value) {

				//$data_t = explode('~', $value['my_answers']);
				$data = explode('~', $value['answers_no']);

				$qn = 1;

				foreach ($data as $answers) {

					$answr = explode('|', $answers);

					if (count($answr) > 1) {
						//for multiple options

						$ch = 'A';
						foreach ($answr as $ans) {
							$jb = explode('-', $ans);
							if (count($jb) > 1) {
								$myqn['Q' . $qn . $ch . 'y'] = $jb[0];
								$myqn['Q' . $qn . $ch . 'm'] = $jb[1];
								$myqn['Q' . $qn . $ch . 'd'] = $jb[2];

							} else {
								$myqn['Q' . $qn . $ch] = $ans;

							}

							$ch++;
						}
					} else {
						//check if data is date
						$jb = explode('-', $answr[0]);
						if (count($jb) > 1) {
							$myqn['Q' . $qn . 'y'] = $jb[0];
							$myqn['Q' . $qn . 'm'] = $jb[1];
							$myqn['Q' . $qn . 'd'] = $jb[2];

						} else {
							$myqn['Q' . $qn] = $answr[0];
						}
						if (strpos($answr[0], '-') !== false) {

						}

					}
					$qn++;
				}

				$myqn;

				unset($value['answers_no']);
				unset($value['my_answers']);
				$value = array_merge($value, $myqn);
				$participants[] = $value;
			}
		}
		//print_r($participants);
		 $this -> exportingToExcel($participants, $flag);
	}

	public function read_summary($where) {
		$intervals = array('today', 'week', 'month', 'total');
		$total = array();
		foreach ($intervals as $interval) {

			if ($interval == 'today') {
				$where_s['participants.interview_date'] = date('Y-m-d');
				$where_r = array_merge($where, $where_s);

			} elseif ($interval == 'week') {
				$date = date('Y-m-d', strtotime(date('Y-m-d')) - (24 * 3600 * 7));
				$where_w['participants.interview_date >='] = $date;
				$where_w['participants.interview_date <='] = date('Y-m-d');
				$where_r = array_merge($where, $where_w);
			} elseif ($interval == 'month') {
				$date = date('Y-m-d', strtotime(date('Y-m-d')) - (24 * 3600 * 31));
				$where_m['participants.interview_date >='] = $date;
				$where_m['participants.interview_date <='] = date('Y-m-d');
				$where_r = array_merge($where, $where_m);
			} elseif ($interval == 'total') {
				$where_r = $where;
			}

			$totals = $this -> reports_model -> count_participants($where_r);
			if ($totals) {
				$total[$interval] = $totals[0]['total'];
			}
		}

		return $total;
	}

}
