<?php
class Reports_model extends MY_Model {
	public $table = 'participants';
	public $table_id = 'participant_id';

	function __construct() {
		parent::__construct();
	}

	public function form_validation() {

		$this -> form_validation -> set_rules('username', 'username', 'trim|required|');

	}

	public function get_participants($where, $surveyId = null) {

		/*
		SELECT * FROM (`pro_participants`) INNER JOIN `pro_surveys` ON 
		`survey_id` = `survey_no` INNER JOIN  `pro_responses` ON 
		`pro_participants`.`participant_no` = `pro_responses`.`participant_no` WHERE
 		`pro_responses`.`survey_no` = '1' 
		AND `pro_participants`.`pmtct_status` = '1' GROUP BY `pro_responses`.`participant_no`;
		*/

		$this -> db -> select('*');
		$this -> db -> from($this -> table);
		$this->db->group_by('pro_responses.participant_no');
		if($surveyId)
			$this -> db -> where("responses.survey_no", $surveyId);
		if ($where) {
			$this -> db -> where($where);

		}
		//	$this -> db -> join('locations', 'loscations.location_code  = participants.location_code', 'left');
		$this -> db -> join('pro_surveys', 'survey_id  = survey_no', 'inner');
		$this -> db -> join('pro_responses', 'pro_responses.participant_no  = pro_participants.participant_no', 'inner');


		$data = $this -> db -> get() -> result_array();

		if (!$data) {
			$data = FALSE;
		}
		return $data;
	}

	public function mycounter($where = False) {
		$this -> db -> select("count($this->table_id) as total");
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$results = $this -> db -> get() -> result_array();
		foreach ($results as $results) {
			return $results['total'];
		}

	}

	public function facility_counter($where = False) {
		$this -> db -> select("count($this->table_id) as total,location");
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> group_by('location');
		$results = $this -> db -> get() -> result_array();
		$facility = array();
		foreach ($results as $key => $value) {
			$facility[$value['location']] = $value['total'];
		}
		return $facility;

	}

	public function participants_counter($where = False) {
		$this -> db -> select("count($this->table_id) as total");
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$results = $this -> db -> get() -> result_array();

		foreach ($results as $key => $value) {

			return $value;
		}

	}

	public function read_surveyor_summary($where = False) {
		$this -> db -> select("count($this->table_id) as total,device_imei");
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> group_by('device_imei');
		$results = $this -> db -> get() -> result_array();

		return $results;

	}

	public function read_surveys_summary($where = False) {
		$this -> db -> select("count($this->table_id) as total,survey_no");
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> group_by('survey_no');
		$results = $this -> db -> get() -> result_array();
		return $results;

	}

	public function read_locations_summary($where = False) {
		$this -> db -> select("count($this->table_id) as total,location_code");
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> group_by('location_code');
		$results = $this -> db -> get() -> result_array();
		return $results;

	}

	public function responses_data($where = false) {

		$this -> db -> select('responses.device_imei,responses.participant_no,
		CONCAT(pro_responses.participant_no,"_",interviewer_no) as participant_no, 
		interviewer_no, responses.survey_no, participants.location_code, response_date,pmtct_status,anc_number,start_time,end_time, GROUP_CONCAT(response_no  order by response_qn  SEPARATOR "~") as answers_no,
		GROUP_CONCAT(response_qn order by response_qn SEPARATOR "~") as questions', FALSE);

		$this -> db -> from('responses');
		if ($where) {
			$this -> db -> where($where);
		}

		// $this -> db -> where(array('responses.participant_no'=>889));

		$this -> db -> join('participants', 'participants.participant_no = responses.participant_no', 'inner'); // Achyut
		//$this -> db -> join('surveys', 'survey_id = responders.survey_no', 'left');
		$this -> db -> group_by('responses.participant_no');
		$this -> db -> order_by('participant_no', 'asc');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function responses_data_with_conditions($where = FALSE, $where_in = FALSE) {

		$this -> db -> select('responses.device_imei,responses.participant_no,interviewer_no, responses.survey_no,    participants.location_code, response_date,pmtct_status,anc_number,start_time,end_time, GROUP_CONCAT(response_no  order by response_qn  SEPARATOR "~") as answers_no,GROUP_CONCAT(response_qn SEPARATOR "~") as questions', FALSE);

		$this -> db -> from('responses');
		if ($where) {
			$this -> db -> where($where);
		}
		if ($where_in) {
			$this -> db -> where_in('responses.participant_no', $where_in);
		}

		$this -> db -> join('participants', 'participants.participant_no = responses.participant_no', 'inner'); // Changed by Achyut

		$this -> db -> group_by('responses.participant_no');
		$this -> db -> order_by('participant_no', 'asc');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function get_open_responses($where, $where_in) {
		$this -> db -> select('responder,survey_no,location,responce_date, GROUP_CONCAT(responce SEPARATOR "|") as responce', FALSE);
		$this -> db -> from('responses');
		if ($where) {
			$this -> db -> where($where);
		}
		if ($where_in) {

			$this -> db -> where_in('responce_qn', $where_in);
		}
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function read_responses($where) {
		$this -> db -> select('*');
		
		$this -> db -> from('responses');
		
		$this->db->join('participants', 'participants.participant_no = responses.participant_no');

		if ($where) {
			$this -> db -> where($where);
		}
		//$this -> db -> where('');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function count_responses($where) {
		$this -> db -> select('count(responce_id) as responce ,responce_qn');
		$this -> db -> from('responses');
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> group_by('response_qn');
		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function count_participants($where) {

		$this -> db -> select('count(*) as total');
		$this -> db -> from($this -> table);
		if ($where) {
			$this -> db -> where($where);
		}

		$data = $this -> db -> get() -> result_array();

		if (!$data) {
			$data = FALSE;
		}
		return $data;
	}

	public function read_raw_data($where = false) {

		$this -> db -> select('responses.device_imei, responses.participant_no,interviewer_no,    participants.location_code, response_date,pmtct_status,anc_number,start_time,end_time,responses.survey_no,response_qn, response,response_no');
		//$this -> db -> select('responses.device_imei, responses.participant_no,     response_date,responses.survey_no,response_qn, response,response_no');
		$this -> db -> from('responses');

		if ($where) {
			$this -> db -> where($where);
		}

		//$this -> db -> join('participants', 'participants.participant_no = responses.participant_no', 'inner'); // Achyut comment 2015.06.09
		$this -> db -> join('participants', 'participants.participant_no = responses.participant_no AND pro_responses.device_imei = pro_participants.device_imei', 'inner');
		//$this -> db -> join('questionaires', 'questionaires.question_rank = response_qn');
		$this -> db -> order_by('response_date', 'asc');

		$data = $this -> db -> get() -> result_array();
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function read_surveyors($where = false) {
		$this -> db -> select('devices.device_id,employees.mobile,staff_no,devices.device_imei,users.location,data_collector_no,users.device_imei as device, CONCAT(firstname ," ", lastname) as employee_name',FALSE);
		$this -> db -> from('employees');
		if ($where) {
			$this -> db -> where($where);
		}
		$this -> db -> join('users', 'users.employee_id =employees.employee_id', 'inner');
		$this -> db -> join('devices', 'device_id =users.device_imei', 'inner');
		$results = $this->db->get()->result_array();
		if($results){return $results;}else{return FALSE;}
	}

}
