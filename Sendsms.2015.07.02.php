<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Keys Controller
 *
 * This is a basic Key Management REST controller to make and delete keys.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php
require(APPPATH.'/libraries/REST_Controller.php');

class Sendsms extends REST_Controller
{
	
	function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller
        date_default_timezone_set('Africa/Nairobi');
        $this->MESSAGE_CODE = array(
             0 => array('code' => 100, 'message' => 'Successful (placed message in queue for delivery)'),
             1 => array('code' => -1, 'message' => 'Invalid XML format'),
             2 => array('code' => -2, 'message' => 'Not enough credits in account'),
             3 => array('code' => -3, 'message' => 'Invalid API key'),
             4 => array('code' => -4, 'message' => 'Destination Mobile number missing / Invalid format'),
             5 => array('code' => -5, 'message' => 'SMS text missing'),
             6 => array('code' => -6, 'message' => 'Sender name missing / invalid format / Not active in account'),
             7 => array('code' => -7, 'message' => 'Network not covered'),
             8 => array('code' => -8, 'message' => 'Error-Undefined'),
             9 => array('code' => -9, 'message' => 'Invalid message id, too long (max 36 chars) or contains non numeric character'),
             10 => array('code' => -10, 'message' => 'Maximum number of recipient in one single API call is 100'),
             11 => array('code' => -11, 'message' => 'Error -11'),
             12 => array('code' => -12, 'message' => 'Message too long (max 160 char)'),
             13 => array('code' => -13, 'message' => 'Invalid username/password'),
             14 => array('code' => -14, 'message' => 'Invalid send time')
                    ); 
		
			$this->load->model(array('Smsprocesslib_model','Credit_model'));

        // $this->data can be accessed from anywhere in the controller.
    }  

	// --------------------------------------------------------------------
	/**
	 * Regenerate Key
	 * Remove a key from the database to stop it working.
	 * @access	public
	 * @return	void
	 */
	public function index_post() {
		/*
		Process Flow:
		1) Incoming sms received
		2) Check if user is authorized
		3) Check if Sender Name exists in system for that vendor_id and is active
		4) Check if GSM number is in correct format
		5) Remove 255 prefix from GSM number
		6) Calculate number of messages (based on character count)
		7) Check if user has enough credits
		8) Insert dummy record into sms_stores table to record history (credit column = message count)
		9) Insert record into inbound_messages table
		10) Return appropriate error/success message
		*/
		//$postdata = file_get_contents("php://input");
		//var_dump( $postdata );exit;

		$dataArr =  array();
		$dataArr['sendername'] = trim($this->post('sendername'));
		$dataArr['username'] = trim($this->post('username'));
		$dataArr['password'] = trim($this->post('password'));
		$dataArr['apikey'] = trim($this->post('apikey'));
		$dataArr['email'] = trim($this->post('email'));
		$dataArr['senddate'] = trim($this->post('senddate'));
		
		 //var_dump($dataArr); exit;
		$arrUser = array();
		$smsStoreBatch = array();
		$smsInboundBatch = array();
		
		$messagCost = 0;

		$arrUser = $this->Smsprocesslib_model->Login($dataArr['email'], $dataArr['username'], md5($dataArr['password']));

		if($arrUser){
			if ( $dataArr['email'] && ($arrUser['id'] != $dataArr['apikey']) )
                  	//die('ErrorCode : 3');
				
				$this->response( array('status' => $this->MESSAGE_CODE[3]['code'], 
							  'message' => $this->MESSAGE_CODE[3]['message']), 200);

          		}
		else
				$this->response( array('status' => $this->MESSAGE_CODE[13]['code'], 
							  'message' => $this->MESSAGE_CODE[13]['message']), 200);

	
		$bulk = $this->post('message');
		
		if( !array_key_exists(1, $bulk) ) //If Single or Multiple
			$bulk = array($bulk);
		
		//var_dump( $bulk ); exit;
		$messageCredit = 0;
		$mobileRet = array();
		
		// Get Available Credit Balance
		$creditBalance = $this->Smsprocesslib_model->getCreditBalance( $arrUser['id'] ); 
		
		foreach($bulk as $blk){
			//echo 'Outside'. $blk['mobile'];
						
			//check destination number
            $validMobile = $this->Smsprocesslib_model->validateAndFormatMobile( $blk['mobile'],  $arrUser);

           if($validMobile == false){
           		array_push($mobileRet, array('msgId'=>$blk['msgId'],'status'=>false));           		
           }
            else {
              	array_push($mobileRet, array('msgId'=>$blk['msgId'],'status'=>true,'msg'=>$blk['text'],'Cell'=>$validMobile ));
		$messagCost = $this->Smsprocesslib_model->getCredit( strlen($blk['text']) );
              	$messageCredit += $messagCost;
            }
            

            
           	

            //checking SMS route to send sms to
			$smsRoute = $this->Smsprocesslib_model->getSMSRoute( $arrUser['sms_route'] );

           // echo $this->db->last_query(); 
			if( $smsRoute == false){
				unset($smsRoute);
            	$smsRoute = $this->Smsprocesslib_model->getDefaultSMSRoute();
            	}


       	       $creditLeft = (int)$creditBalance - (int)$messageCredit;
       	       //var_dump(( ($creditBalance <= 0) or ($creditLeft < 0) ));exit;
       	          if ( ($creditBalance <= 0) or ($creditLeft <= 0) )
                                {
                                   //die('ErrorCode : 2');
                                	$this->response( array('error' => $this->MESSAGE_CODE[2]['code'], 
							  'message' => $this->MESSAGE_CODE[2]['message']), 200);
                                }
               
                $arrSender = $this->Smsprocesslib_model->getActiveSenderid( $arrUser['id'] );
		//var_dump($arrSender);
                $arrSenderName = '';
                if ($arrSender) {
                                 /* check sender name */
                                 $arrSenderName = $this->Smsprocesslib_model->searchSenderName($arrSender, $dataArr['sendername']);
                                 //var_dump( $arrSenderName );exit;
                                }
		//var_dump($arrSenderName);exit;
                 if (!$arrSenderName) {
                 	//die('ErrorCode $arrSenderName : 6');
			$this->response( array('error' => $this->MESSAGE_CODE[6]['code'], 
							  'message' => $this->MESSAGE_CODE[6]['message']), 200);
		

                 }
	
                /* INSERT INTO sms_stores */
				$smsStoreID = 0;
                		//if(!$smsStoreID){
				$this->db->insert('sms_stores', array(
									'sent_by'=>$arrUser['id'],
									'sender_id'=>$arrSenderName['id'],
									'prev_balance'=>$creditBalance,
									'credit' =>$messageCredit,
									'addressbook_id'=>'API',						
									'contact_id' =>'API',
									/*'receiver_id' =>$receiverId,
									'receiver_type' =>$receiverType,*/
									'sender_type' =>'VEN',
									'advertiser_id' =>0,
									'text_message' =>$this->db->escape_str( $blk['text'] ),
									'message_length' =>strlen( $blk['text'] ),
									'message_count' =>$messagCost,
									'is_proccessed' => 0,
									'is_shceduled' => ( empty($dataArr['senddate']) ) ? 0 : 1,
									'send_time'=>$dataArr['senddate'],		               
									'sms_route' => $smsRoute['route_name'],
									'sms_priority' => $arrUser['sms_priority'],
									'post_method' => 'XML' ) );
					//$smsStoreID = $this->db->insert_id();
					 	
					$smsStoreID = $this->Smsprocesslib_model->getSmsStoresIDapi($arrUser['id'], $arrSenderName['id'], 
											$blk['text'],$validMobile);		
			
				//	}

				$arrClient = array();				
				//$smsStoreID = $this->Smsprocesslib_model->getSmsStoresIDapi($arrUser['id'], $arrSenderName['id'], $blk['text'],$validMobile);

				   if ($smsStoreID) {
               					
               					$arrClient = $this->Smsprocesslib_model->getClientNetworksRoutes($arrUser['id']);
                           }
           		
			//var_dump($arrClient);
                 	if($arrClient){
			//echo "here";
							$arrClintNetworkNames = array();
							$arrNetworkIds = array();
							foreach($arrClient as $kiClient){
									array_push($arrClintNetworkNames, $kiClient['name']);
									array_push($arrNetworkIds, $kiClient['network_id']);
									}
							$strNetworknames = implode("','", $arrClintNetworkNames);
							$strNetworkids = implode("','",$arrNetworkIds);
							
							$arrAll = $this->Smsprocesslib_model->getNetworksRoutesUpdated($strNetworknames, $strNetworkids);
							$arrNetworks = array_merge($arrClient, $arrAll);
						}else{
							//get all networks prefixes
							$arrNetworks = $this->Smsprocesslib_model->getNetworksRoutes();
							}

					//get prefix of the number
					$prefix = substr( $validMobile ,0 ,5);
					//var_dump($arrNetworks);exit;
					$routeToUse = null;
					$networkName = null;
						
					//finding the network in prefix in which prefix belong
					foreach($arrNetworks as $kinetwork){
						$arrPrefix = explode(",",$kinetwork['prefix']);
						//var_dump($arrPrefix);
							if (in_array($prefix, $arrPrefix , true)) {
								$routeToUse = $kinetwork['route_name'];
								$networkName = $kinetwork['name'];
									}
							unset($arrPrefix);
						}
					//echo $routeToUse;exit;
				//checking if route where given for particular prefix
					if($routeToUse==""){
						$routeToUse ="Uncovered Network";
						}					                               

					if($validMobile){		               								 array_push($smsInboundBatch, array( 
                    					     'sms_store_id'=>$smsStoreID,
                    						 'user_id'=>$arrUser['id'],
                    						 'sms_route'=>$routeToUse,
                    						 'network_name'=>$networkName,
                    						 'status'=>0,
                    						 'short_message'=>$blk['text'],
                    						 'msg_length'=>strlen($blk['text']),
                    						 'msg_count'=>$messagCost,
                    						 'source_addr'=>$arrSenderName['senderid'],
                    						 'destination_addr'=>$validMobile
                    					 ));
						}
                   

		} //Foreach Message Packet

		//echo $messageCredit;exit;
		//var_dump($smsInboundBatch);exit;
		
		$this->db->trans_begin();

		$this->Smsprocesslib_model->blockCredit($messageCredit, $arrUser['id']);
		if(count($smsInboundBatch) > 0)		
			$this->db->insert_batch('inbound_messages', $smsInboundBatch );
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->response( array('error' => $this->MESSAGE_CODE[1]['code'],
			 'message' => $this->MESSAGE_CODE[1]['message']), 200);
		}
		else  {
		$this->db->trans_commit();
		$this->response( array('status' => $this->MESSAGE_CODE[0]['code'], 
							  'message' => $this->MESSAGE_CODE[0]['message'],'mobileList'=>$mobileRet), 200);
		}
		
	}

}
